#ifndef DIGICLOCK_H
#define DIGICLOCK_H

#include <QObject>
#include <QLCDNumber>
class DigiClock : public QLCDNumber
{
    Q_OBJECT
public:
    DigiClock(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private slots:
    void showTime();

private:
    QPoint dragPosition;
    bool showColon;
};

#endif // DIGICLOCK_H
